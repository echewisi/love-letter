import java.util.*;
import java.util.stream.Collectors;

    public class Spiel {

            private ArrayList<Spieler> spielerListe;
            private Deck deck;
            private boolean spielLaeuft;
            private Spieler startSpieler;
            private GameState gameState = GameState.INITIALIZING;
            private static final Scanner scanner = new Scanner(System.in);
            private int aktuellerSpielerIndex;

            private static final int TOKENS_TO_WIN_FOR_TWO_PLAYERS = 7;
            private static final int TOKENS_TO_WIN_FOR_THREE_PLAYERS = 5;
            private static final int TOKENS_TO_WIN_FOR_FOUR_PLAYERS = 4;
            private static final int MAX_PLAYERS = 4;
            private static final int MIN_PLAYERS = 2;

        public enum GameState {
            INITIALIZING,
            RUNNING,
            STARTED, FINISHED
        }
        public Spiel() {
            if (this.gameState != GameState.INITIALIZING) {
                throw new IllegalStateException("Spiel kann nicht gestartet werden im Zustand: " + this.gameState);
            }
                spielerListe = new ArrayList<>();
                deck = new Deck();
                spielLaeuft = false;
            this.gameState = GameState.RUNNING;
            }

        public void initSpiel() {

            System.out.println("Sie spielen das Love Letter Spiel:)" +
                    " Wie viele Spieler? (2-4): ");

            int spielerAnzahl = 0;
            while (spielerAnzahl < 2 || spielerAnzahl > 4) {
                if (scanner.hasNextInt()) {
                    spielerAnzahl = scanner.nextInt();
                    scanner.nextLine(); // Consume newline left-over
                    if (spielerAnzahl < 2 || spielerAnzahl > 4) {
                        System.out.println("Ungültige Anzahl. Bitte wählen Sie zwischen 2 und 4 Spielern.");
                    }
                } else {
                    System.out.println("Ungültige Eingabe. Bitte geben Sie eine Zahl zwischen 2 und 4 ein.");
                    scanner.next(); // Consume the invalid token to avoid an infinite loop
                }
            }

            spielerListe = new ArrayList<>(); // Initialisieren der Spielerliste, falls noch nicht geschehen
            for (int i = 0; i < spielerAnzahl; i++) {
                System.out.print("Geben Sie den Namen für Spieler " + (i + 1) + ": ");
                String spielerName = scanner.nextLine();
                spielerListe.add(new Spieler(spielerName));
            }

            aktuellerSpielerIndex = 0; // Setzen des aktuellen Spielerindex auf den ersten Spieler

            System.out.println("Spiel wurde initialisiert. Nutzen Sie \\help für eine Liste der Befehle.");
        }



            public void starteSpiel() {
                spielLaeuft = true;
                deck.mischen();
                while (spielLaeuft) {
                    for (Spieler spieler : spielerListe) {
                        if (!spieler.isAktiv()) {
                            continue;
                        }

                        if (istDeckLeer()) {
                            rundeBeenden();
                            break;
                        }

                        spielerTurn(spieler);

                        if (istDeckLeer() || pruefeRundenEnde()) {
                            rundeBeenden();
                            break;
                        }
                    }
                }

                spielBeenden();
            }
        private boolean istDeckLeer() {
            if (deck.istLeer()) {
                System.out.println("Keine Karten mehr im Deck. Die Runde wird beendet.");
                return true;
            }
            return false;
        }

        private void spielerTurn(Spieler spieler) {
            // Remove Handmaid protection at the start of the player's turn
            if (spieler.isGeschuetzt()) {
                spieler.setGeschuetzt(false);
                System.out.println(spieler.getName() + " ist nicht mehr durch die Zofe geschützt.");
            }
            Karte gezogeneKarte = deck.zieheKarte();
            if (gezogeneKarte != null) {
                spieler.addKarteToHand(gezogeneKarte);
                karteSpielen(spieler);
            } else {
                // If no card can be drawn (deck is empty), end the round
                rundeBeenden();
            }
        }

        private void wendeKartenEffekteAn(Karte karte, Spieler spieler) {
            Spieler targetSpieler;
            switch(karte.getRang()) {
                case 1: // Guard
                    // Get a guess from the player
                    int guess = spieler.getGuess();

                    // Ask which player they want to guess
                    targetSpieler = this.chooseTargetPlayer(spielerListe);

                    // First, check if the target player is protected by the "Zofe"
                    if (targetSpieler != null && targetSpieler.isGeschuetzt()) {
                        System.out.println(targetSpieler.getName() + " ist durch die Zofe geschützt und kann nicht als Ziel gewählt werden.");
                    } else if (targetSpieler != null && targetSpieler.hasCardWithRank(guess)) {
                        // If the player is not protected, check if the guess is correct
                        this.eliminatePlayer(targetSpieler);
                        System.out.println(targetSpieler.getName() + " wurde richtig erraten und ist nun ausgeschieden.");
                    } else {
                        // If the guess is incorrect
                        System.out.println("Die Vermutung war falsch. Kein Spieler wird eliminiert.");
                    }
                    break;

                case 2: // Priest
                    // Ask the current player to select another player to view their hand
                    targetSpieler = this.chooseTargetPlayer( spielerListe);

                    // Make sure the target player is not null and is not eliminated
                    if (targetSpieler != null && !targetSpieler.isEliminated()) {
                        // The current player views the hand of the target player
                        Karte targetHandKarte = targetSpieler.getHandCard();
                        System.out.println(targetSpieler.getName() + " ist durch die Zofe geschützt und kann nicht ausgewählt werden.");
                    } else if (targetSpieler != null && !targetSpieler.isEliminated()) {
                        // The current player views the hand of the target player
                        Karte targetHandKarte = targetSpieler.getHandCard();
                        // Show the card to the current player without revealing to others

                        // Show the card to the current player without revealing to others
                        spieler.viewCard(targetHandKarte);
                    }
                    break;
                case 3: // Baron
                    // Wählen Sie einen anderen Spieler für den Effekt des Barons
                    Spieler zielSpieler = chooseTargetPlayer(spielerListe);

                    // Vergleichen Sie die Handkarten, wenn zielSpieler nicht null ist
                    if (zielSpieler != null) {
                        Karte spielerKarte = spieler.getHandCard();
                        Karte zielSpielerKarte = zielSpieler.getHandCard();

                        // Der Spieler mit der niedrigeren Karte scheidet aus, bei einem Unentschieden passiert nichts
                        if (spielerKarte.getRang() > zielSpielerKarte.getRang()) {
                            // zielSpieler scheidet aus
                            eliminatePlayer(zielSpieler);
                        } else if (spielerKarte.getRang() < zielSpielerKarte.getRang()) {
                            // spieler scheidet aus
                            eliminatePlayer(spieler);
                        } // bei einem Unentschieden passiert nichts
                    }
                    break;
                case 4: // Handmaid
                    // Setzen Sie den Schutzstatus des Spielers bis zum nächsten Zug
                    spieler.setGeschuetzt(true);
                    System.out.println(spieler.getName() + " ist jetzt durch die Zofe geschützt bis zum Beginn des nächsten Zuges.");
                    break;

                case 5: // Prince
                // Choose a player (including the current player)
                Spieler target = chooseTargetPlayer(spielerListe);

                // Target player discards their hand
                Karte discardedCard = target.discardHand();
                    if (discardedCard != null) {
                // Check if the discarded card is the Princess
                if (discardedCard.getRang() == 8) {
                    // Apply the effect of the Princess and eliminate the player
                    eliminatePlayer(target);
                } else {
                    // The player must draw a new card if possible
                    if (deck.istLeer()) { // assuming 'deck' is your Deck instance
                        // Draw the card that was removed at the start of the round if the deck is empty
                        Karte removedCard = deck.getRemovedCardAtStart(); // call the method on the 'deck' instance
                        target.addKarteToHand(removedCard);
                    } else {
                        // Otherwise, draw a card from the deck
                        Karte newCard = deck.zieheKarte(); // assuming 'deck' is your Deck instance
                        target.addKarteToHand(newCard);
                    }
                }
                    }
                break;
                case 6: // King
                    // Check for valid players to trade with
                    List<Spieler> validPlayers = getValidPlayersForTrade(spieler ); // Assuming 'game' is the instance of your game control class

                    if (!validPlayers.isEmpty()) {
                        // Allow current player to choose a player to trade with
                        Spieler chosenPlayer = spieler.choosePlayer(validPlayers); // This method must be in the 'Spieler' class

                        if (chosenPlayer != null) {
                            // Perform the trade
                            Karte currentPlayerCard = spieler.getCurrentCard(); // This method must be in the 'Spieler' class
                            Karte chosenPlayerCard = chosenPlayer.getCurrentCard(); // This method must be in the 'Spieler' class

                            spieler.setCurrentCard(chosenPlayerCard);
                            chosenPlayer.setCurrentCard(currentPlayerCard);

                            // Log or notify the players of the trade
                            System.out.println(spieler.getName() + " hat die Handkarten mit " + chosenPlayer.getName() + " getauscht.");
                        }
                    } else {
                        // Log or handle the case where there is no valid player to trade with
                        System.out.println("Es gibt keinen Spieler, mit dem " + spieler.getName() + " die Handkarten tauschen kann.");
                    }
                    break;
            }

        }
        private List<Spieler> getValidPlayersForTrade(Spieler spieler) {
            // Assuming spielerListe contains all the players in the game
            return spielerListe.stream()
                    .filter(s -> !s.equals(spieler)) // exclude the current player
                    .filter(s -> !s.isEliminated()) // exclude eliminated players
                    .filter(s -> !s.isGeschuetzt()) // exclude players protected by Handmaid
                    .collect(Collectors.toList());
        }


        public Spieler chooseTargetPlayer(List<Spieler> eligiblePlayers) {
            int selectedPlayerIndex = -1;
            while (selectedPlayerIndex < 0 || selectedPlayerIndex >= eligiblePlayers.size()) {
                System.out.print("Wähle die Nummer des Spielers: ");
                try {
                    selectedPlayerIndex = Integer.parseInt(scanner.nextLine()) - 1;
                } catch (NumberFormatException e) {
                    System.out.println("Ungültige Eingabe. Bitte geben Sie eine gültige Spieler-Nummer ein.");
                }
            }
            return eligiblePlayers.get(selectedPlayerIndex);

        }
        private List<Spieler> getEligiblePlayers(Spieler currentPlayer, List<Spieler> allPlayers) {
            List<Spieler> eligiblePlayers = new ArrayList<>();

            // Iterate over the list of all players to find eligible players
            for (Spieler player : allPlayers) {
                // Your conditions for whether a player is eligible
                if (!player.isEliminated() && player != currentPlayer && !player.isGeschuetzt()) {
                    eligiblePlayers.add(player);
                }
            }

            if (eligiblePlayers.isEmpty()) {
                System.out.println("Es gibt keine verfügbaren Ziele. Die Karte hat keinen Effekt.");
                return null;
            }

            // Prompt the current player to choose a target from eligible players
            System.out.println("Wähle einen Spieler, den du verdächtigen möchtest:");
            for (int i = 0; i < eligiblePlayers.size(); i++) {
                System.out.println((i + 1) + ": " + eligiblePlayers.get(i).getName());
            }

            int selectedPlayerIndex = -1;
            while (selectedPlayerIndex < 0 || selectedPlayerIndex >= eligiblePlayers.size()) {
                System.out.print("Wähle die Nummer des Spielers: ");
                if (scanner.hasNextInt()) {
                    selectedPlayerIndex = scanner.nextInt() - 1; // The index needs to be 0-based
                    scanner.nextLine(); // Consume newline
                } else {
                    scanner.nextLine(); // Consume invalid input
                    System.out.println("Ungültige Eingabe. Bitte geben Sie eine gültige Spieler-Nummer ein.");
                }
            }

            // Assuming you want to return a single player, not the list of all eligible players
            return eligiblePlayers;
        }
        public void eliminatePlayer(Spieler spieler) {
            // Markieren Sie den Spieler als ausgeschieden
            spieler.markiereAlsAusgeschieden();

            // Entfernen Sie den Spieler aus der aktiven Spielerliste
            spielerListe.remove(spieler);

            // Wenn nur noch ein Spieler übrig ist, könnte dies das Ende des Spiels bedeuten
            if (spielerListe.size() == 1) {
                // Spiel zu Ende, wenn nur ein Spieler übrig ist
                spielLaeuft = false;
                spielBeenden();
            }

        }
        private void resetGameForNextRound() {
            // Shuffle cards and prepare the deck
            // Shuffle cards and prepare the deck
            deck.mischen();
            spielerListe.forEach(Spieler::leereAblagestapel);

            // Assuming you have the number of tokens needed to win the game as a variable
            int winningTokensNeeded = getWinningTokensNeeded(); // You need to implement this method

            // Reset each player for the new round with the number of tokens needed to win
            spielerListe.forEach(spieler -> spieler.resetZumNeuenRunde(winningTokensNeeded));

            // Now that everything is reset, we determine the starting player for the next round
            List<Spieler> roundWinners = getRoundWinners(); // logic to determine round winners
            ermittleStartSpielerFuerNaechsteRunde(roundWinners);

        }
        private int getWinningTokensNeeded() {
            // The number of players is determined by the size of the spielerListe.
            int numberOfPlayers = spielerListe.size();
            switch (numberOfPlayers) {
                case 2:
                    return 7; // Two players need 7 tokens to win.
                case 3:
                    return 5; // Three players need 5 tokens to win.
                case 4:
                    return 4; // Four players need 4 tokens to win.
                default:
                    // If there's an unexpected number of players, you can throw an exception
                    throw new IllegalStateException("Unexpected number of players: " + numberOfPlayers);
            }
        }


            public void ermittleStartSpielerFuerNaechsteRunde(List<Spieler> winners) {
                Scanner scanner = new Scanner(System.in); // Scanner should only be initialized once, consider moving this to a class level and closing it when the application ends

                if (winners.size() == 1) {
                    // If there is only one winner, they start the next round
                    startSpieler = winners.get(0);
                } else if (winners.size() > 1) {
                    // If there was a tie, ask which of the tied players was most recently on a date
                    System.out.println("Bitte den Namen des Spielers eingeben, der zuletzt ein Date hatte:");
                    String lastDatePlayerName = scanner.nextLine();

                    // Now find this player in the list of winners
                    startSpieler = winners.stream()
                            .filter(spieler -> spieler.getName().equalsIgnoreCase(lastDatePlayerName.trim()))
                            .findFirst()
                            .orElse(null);
                }

                if (startSpieler != null) {
                    System.out.println(startSpieler.getName() + " wird als Erster in der nächsten Runde beginnen.");
                } else {
                    System.out.println("Spieler nicht gefunden. Standardregeln zur Bestimmung des Startspielers anwenden.");
                    // You might want to apply some default rules here if the input did not match any player
                }
                scanner.close();
            }

        private List<Spieler> getRoundWinners() {
            List<Spieler> winners = new ArrayList<>();

            // First, find the player(s) with the highest hand card value.
            int highestValue = spielerListe.stream()
                    .filter(Spieler::isInRound)
                    .mapToInt(spieler -> spieler.getHandCard().getRang())
                    .max()
                    .orElse(0);

            // Add all players with the highest card value to the potential winners list.
            spielerListe.stream()
                    .filter(spieler -> spieler.isInRound() && spieler.getHandCard().getRang() == highestValue)
                    .forEach(winners::add);

            // If there's a tie, we consider the discard piles' values.
            if (winners.size() > 1) {
                int highestDiscardValue = winners.stream()
                        .mapToInt(Spieler::getDiscardTotal)
                        .max()
                        .orElse(0);

                // We only keep the players with the highest discard pile total.
                winners = winners.stream()
                        .filter(spieler -> spieler.getDiscardTotal() == highestDiscardValue)
                        .collect(Collectors.toList());
            }

            // If there's still a tie, all tied players win the round.
            return winners;
        }


        public void setzeStartSpieler(Spieler startingPlayer) {
            // Set the starting player for the round
        }
        // This should be called before a player plays a card
        private boolean checkAndDiscardCountessIfRequired(Spieler spieler) {
            // Check if the player has the Countess
            Karte countess = spieler.getHand().stream()
                    .filter(k -> k.getRang() == 7)
                    .findAny()
                    .orElse(null);

            // Check if the player also has the King or Prince
            boolean hasRoyal = spieler.getHand().stream()
                    .anyMatch(k -> k.getRang() == 6 || k.getRang() == 5);

            // If the player has both the Countess and a royal family member, they must discard the Countess
            if (countess != null && hasRoyal) {
                spieler.discardCard(countess);
                // Output for the player to know what happened
                System.out.println(spieler.getName() + " muss die Gräfin abwerfen, da sie auch einen König oder Prinzen in der Hand hat.");
                return true; // Indicate that the Countess was discarded
            }

            return false; // Indicate no forced discard occurred
        }



        private void zeigeAbgelegteKarte(Spieler spieler, Karte karte) {
            // Logik, um die abgelegte Karte zu zeigen. Zum Beispiel:
            System.out.println(spieler.getName() + " hat die folgende Karte abgelegt: " + karte);
        }

        private void rundeBeenden() {
            System.out.println("Die Runde ist vorbei. Punkte werden berechnet...");
            // Find the player with the highest rank card in hand.


            // Find the player with the highest rank card in hand.
            Spieler winningPlayer = null;
            List<Spieler> highestRankPlayers = new ArrayList<>();
            int highestRank = 0;

            for (Spieler spieler : spielerListe) {
                if (spieler.isInRound() && spieler.getHandCard().getRang() > highestRank) {
                    highestRank = spieler.getHandCard().getRang();
                    highestRankPlayers.clear();
                    highestRankPlayers.add(spieler);
                } else if (spieler.isInRound() && spieler.getHandCard().getRang() == highestRank) {
                    highestRankPlayers.add(spieler);
                }
            }

            // Check for ties among the remaining players.
            List<Spieler> tiedPlayers = new ArrayList<>();
            for (Spieler spieler : spielerListe) {
                if (spieler.isInRound() && spieler.getHandCard().getRang() == highestRank) {
                    tiedPlayers.add(spieler);
                }
            }

            // Calculate total discard values in case of a tie.
            if (tiedPlayers.size() > 1) {
                int highestDiscardTotal = 0;
                for (Spieler spieler : tiedPlayers) {
                    int discardTotal = spieler.getDiscardTotal(); // This method needs to calculate the total of discarded cards.
                    if (discardTotal > highestDiscardTotal) {
                        highestDiscardTotal = discardTotal;
                        winningPlayer = spieler;
                    }
                }
            }
            // Handle ties by discard totals if necessary
            if (highestRankPlayers.size() > 1) {
                int highestDiscardTotal = 0;
                List<Spieler> winners = new ArrayList<>();

                for (Spieler spieler : highestRankPlayers) {
                    int discardTotal = spieler.getDiscardTotal(); // Ensure method is properly implemented!!
                    if (discardTotal > highestDiscardTotal) {
                        highestDiscardTotal = discardTotal;
                        winners.clear(); // Clear the list as there is a new highest total
                        winners.add(spieler);
                    } else if (discardTotal == highestDiscardTotal) {
                        winners.add(spieler); // Add to winners as there is a tie
                    }
                }
                highestRankPlayers = winners; // Now highestRankPlayers holds all the winners
            }
            // Now highestRankPlayers list contains all the winners whether it was a tie or not.
            for (Spieler winner : highestRankPlayers) {
                winner.awardToken();
                System.out.println(winner.getName() + " gewinnt die Runde mit einem Gesamtwert der verworfenen Karten von " + winner.getDiscardTotal() + ".");
            }

            // Prepare for the next round.
            resetGameForNextRound();
        }

        private boolean pruefeRundenEnde() {
            // Check if the deck is empty at the end of a player's turn or if only one player remains
            if (deck.istLeer() || spielerListe.stream().filter(Spieler::isAktiv).count() <= 1) {
                rundeBeenden();
                return true;
            }
            return false;
        }

        private void spielBeenden() {
            System.out.println("Das Spiel ist zu Ende. Gewinner wird berechnet...");

            // Determine the number of tokens needed to win based on player count
            int tokensToWin;
            switch (spielerListe.size()) {
                case 2:
                    tokensToWin = 7;
                    break;
                case 3:
                    tokensToWin = 5;
                    break;
                case 4:
                default:
                    tokensToWin = 4;
                    break;
            }

            // Find the winner
            Spieler gewinner = null;
            for (Spieler spieler : spielerListe) {
                if (spieler.getScore() >= tokensToWin) {
                    if (gewinner == null || spieler.getScore() > gewinner.getScore()) {
                        gewinner = spieler;
                    }
                }
            }

           if (gewinner != null) {
                System.out.println("Der Gewinner ist " + gewinner.getName() + " mit " + gewinner.getScore() + " Liebesbeweisen.");
                // End the game or start a new game based on your game's rules
            } else {
                // No winner yet, start a new round
                resetGameForNextRound();
            }
            this.gameState = GameState.FINISHED;
        }

            public void befehleAnzeigen() {
                System.out.println("\\help - Zeigt alle verfügbaren Befehle.");
                System.out.println("\\start - Startet das Spiel.");
                System.out.println("\\playCard - Spielt eine Karte aus der Hand.");
                System.out.println("\\showHand - Zeigt die Hand des aktuellen Spielers.");
                System.out.println("\\showScore - Zeigt den Punktestand aller Spieler.");
                System.out.println("\\showPlayers - Zeigt alle Spieler und ihren Status.");
            }



            public void karteSpielen(Spieler spieler) {
                // Spieler zieht eine Karte vom Deck und fügt sie der Hand hinzu.
                System.out.println("Hand size before drawing a card: " + spieler.getHand().size());
                Karte gezogeneKarte = deck.zieheKarte();
                spieler.addKarteToHand(gezogeneKarte);
                System.out.println("Hand size after drawing a card: " + spieler.getHand().size());


                System.out.println(spieler.getName() + " hat eine Karte gezogen. Wähle eine Karte zum Spielen.");
                spieler.zeigeHand(); // Make sure this method shows exactly two cards.

                // Eingabeaufforderung für den Spieler, eine Karte zu wählen.
                int gewaehlteKarteIndex = -1;
                while (gewaehlteKarteIndex < 0 || gewaehlteKarteIndex > 1) {
                    System.out.println("Wähle 1 oder 2 für die entsprechende Karte in deiner Hand.");
                    if (scanner.hasNextInt()) {
                        gewaehlteKarteIndex = scanner.nextInt() - 1;
                    } else {
                        scanner.next(); // Nicht-int Eingabe verwerfen.
                    }
                }

                // Wähle die Karte, die der Spieler spielen möchte.
                Karte gespielteKarte = spieler.getHand().get(gewaehlteKarteIndex);

                // Wende die Effekte der Karte an.
                wendeKartenEffekteAn(gespielteKarte, spieler);

                // Lege die gewählte Karte ab und entferne sie aus der Hand des Spielers.
                spieler.getHand().remove(gewaehlteKarteIndex);
                System.out.println(spieler.getName() + " legt " + gespielteKarte + " ab und wendet den Effekt an.");

                // Zeige die abgelegte Karte offen an.
                zeigeAbgelegteKarte(spieler, gespielteKarte);
                }


            public void scoreAnzeigen() {
                //  logic to show all players' scores
                System.out.println("Punktestand aller Spieler:");
                for (Spieler spieler : spielerListe) {
                    System.out.println(spieler.getName() + ": " + spieler.getScore() + " Punkte");
                }
            }

            public void spielerAnzeigen() {
                //  logic to show all players and their statuses
                System.out.println("Alle Spieler:");
                for (Spieler spieler : spielerListe) {
                    String status = spieler.isAktiv() ? "aktiv" : "ausgeschieden";
                    System.out.println(spieler.getName() + " - Status: " + status);
                }
            }
        public void zeigeHand(Spieler spieler) {
            // Überprüft, ob die Runde noch läuft
            if (gameState != GameState.FINISHED) {
                System.out.println(spieler.getName() + "'s verdeckte Hand:");
                List<Karte> hand = spieler.getHand();
                for (int i = 0; i < hand.size(); i++) {
                    System.out.println((i + 1) + ". " + hand.get(i).getName() + " (verdeckt)");
                }
            } else {
                System.out.println(spieler.getName() + "'s aufgedeckte Hand:");
                List<Karte> hand = spieler.getHand();
                for (int i = 0; i < hand.size(); i++) {
                    System.out.println((i + 1) + ". " + hand.get(i).getName());
                }
            }
        }
        public Spieler getAktuellerSpieler() {
            return spielerListe.get(aktuellerSpielerIndex);
        }

            public static void main(String[] args) {
                Spiel spiel = new Spiel();
                spiel.initSpiel();


                while (spiel.gameState != GameState.FINISHED) {
                    String eingabe = scanner.nextLine();

                    switch (eingabe) {
                        case "\\help":
                            spiel.befehleAnzeigen();
                            break;
                        case "\\start":
                            spiel.starteSpiel();
                            break;
                        case "\\playCard":
                            // Logik um die aktuelle Hand des Spielers zu bekommen und Karte zu spielen
                            break;
                        case "\\showHand":
                            if (spiel.gameState == GameState.RUNNING || spiel.gameState == GameState.STARTED) {
                                Spieler aktuellerSpieler = spiel.getAktuellerSpieler();
                                spiel.zeigeHand(aktuellerSpieler);
                            } else {
                                System.out.println("Das Spiel ist zu Ende. Keine Hände zu zeigen.");
                            }
                            break;
                        case "\\showScore":
                            spiel.scoreAnzeigen();
                            break;
                        case "\\showPlayers":
                            spiel.spielerAnzeigen();
                            break;
                        default:
                            System.out.println("Unbekannter Befehl. Nutzen Sie \\help, um alle Befehle zu sehen.");
                    }
                }

                scanner.close();
            }
    }