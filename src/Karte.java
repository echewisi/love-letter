public class Karte {
    private String name;
    private int rang;
    private String effekt;

    public Karte(String name, int rang, String effekt) {
        this.name = name;
        this.rang = rang;
        this.effekt = effekt;
    }

    // Getter methods
    public String getName() { return name; }
    public int getRang() { return rang; }
    public String getEffekt() { return effekt; }


    @Override
    public String toString() {
        return "Karte{" +
                "name='" + name + '\'' +
                ", rang=" + rang +
                ", effekt='" + effekt + '\'' +
                '}';
    }
}

