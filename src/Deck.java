import java.util.Collections;
import java.util.Stack;

public class Deck {
    private Stack<Karte> karten;
    private Karte removedCardAtStart;

    public Deck() {
        karten = new Stack<>();
        // Add the specific cards required for Love Letter
        karten.push(new Karte("Princess", 8,"Wenn du diese Karte ablegst, scheidest du aus der Runde aus."));  // Add card effects
        karten.push(new Karte("Countess", 7,"Wenn du diese Karte mit dem King oder Prince auf der Hand hast, musst du sie ablegen."));
        karten.push(new Karte("King", 6,"Tausche die Handkarten mit einem anderen Spieler deiner Wahl."));
        for (int i = 0; i < 2; i++) {
            karten.push(new Karte("Prinz", 5, "Wähle einen Spieler (dich selbst eingeschlossen). Der Spieler muss seine Handkarte ablegen und eine neue ziehen."));
        }
        for (int i = 0; i < 5; i++) {
            karten.push(new Karte("Guard", 1,"Wähle einen anderen Spieler und rate dessen Handkarte.")); // Guards are five copies
        }
        for (int i = 0; i < 2; i++) {
            karten.push(new Karte("Zofe", 4, "Bis zu deinem nächsten Zug können andere Spieler dich nicht als Ziel für ihre Karten wählen."));
        }
        for (int i = 0; i < 2; i++) {
            karten.push(new Karte("Baron", 3, "Vergleiche deine Handkarte mit der eines anderen Spielers. Der Spieler mit dem niedrigeren Wert scheidet aus."));
        }
        for (int i = 0; i < 2; i++) {
            karten.push(new Karte("Priester", 2, "Sieh dir die Handkarte eines anderen Spielers an."));
        }
        for (int i = 0; i < 5; i++) {
            karten.push(new Karte("Wächter", 1, "Wähle einen anderen Spieler und rate dessen Handkarte (außer Wächter). Liegst du richtig, scheidet dieser Spieler aus."));
        }


        mischen(); // i`ll Shuffle all cards here
    }


    public void mischen() {
        Collections.shuffle(karten);
    }

    public Karte zieheKarte() {
        if (!karten.isEmpty()) {
        return karten.pop();
    }
    return null;
}

    public void entferneObersteKarte() {
        // Entferne die oberste Karte ohne sie anzusehen
        if (!karten.isEmpty()) {
            karten.pop();
        }
    }

    public Karte[] entferneDreiKarten() {
        // Zähle, wie viele Karten entfernt werden können (maximal 3).
        int zuEntfernendeAnzahl = Math.min(3, karten.size());

        // Erstelle ein Array nur für die Anzahl der zu entfernenden Karten.
        Karte[] entfernteKarten = new Karte[zuEntfernendeAnzahl];

        for (int i = 0; i < zuEntfernendeAnzahl; i++) {
            entfernteKarten[i] = karten.pop();
        }
        return entfernteKarten;
    }


    public void addKarte(Karte karte) {
        karten.push(karte);
    }

    public boolean istLeer() {
        return karten.isEmpty();
    }
    public Karte getRemovedCardAtStart() {
        return removedCardAtStart;
    }
}