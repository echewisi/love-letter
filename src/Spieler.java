import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Spieler {
    private String name;
    private ArrayList<Karte> hand;
    private boolean aktiv;
    private int score;
    private Scanner scanner = new Scanner(System.in);
    private boolean istAusgeschieden;
    private List<Karte> discardPile = new ArrayList<>();
    private Karte currentCard;
    private boolean geschuetzt;

    public Spieler(String name) {
        this.name = name;
        this.hand = new ArrayList<>();
        this.aktiv = true;
        this.score = 0;
        this.istAusgeschieden = false;
    }
    public String getName() {
        return name;
    }

    public void addKarteToHand(Karte karte) {
        hand.add(karte);
        this.currentCard = karte;
    }

    public boolean isAktiv() {
        return aktiv;
    }
    public void setAktiv(boolean aktiv) {
        this.aktiv = aktiv;
    }
    // Methode zum Anzeigen der Hand könnte hier auch hilfreich sein
    public void zeigeHand() {
        System.out.println("Deine Handkarten:");
        for (int i = 0; i < hand.size(); i++) {
            System.out.println((i + 1) + ": " + hand.get(i));
        }
    }
    public List<Karte> getHand() {
        return new ArrayList<>(hand); // Gibt eine Kopie der Hand zurück
    }
    public int getScore() {
        return score;
    }

    // Methode zum Hinzufügen von Punkten
    public void addScore(int points) {
        this.score += points;
    }
    public int getGuess() {
        // Method to prompt the player to make a guess
            int guess;
            do {
                System.out.println("Guess the card number (2-8): ");
                while (!scanner.hasNextInt()) {
                    System.out.println("That's not a number. Please enter a number between 2 and 8.");
                    scanner.next(); // Move the scanner past the current input
                }
                guess = scanner.nextInt();
            } while (guess == 1 || guess < 2 || guess > 8); // Ensuring the guess is not 1 and is within the valid range

            return guess;
        }
    // Method to check if the player has a card with a specific rank
    public boolean hasCardWithRank(int rank) {
        for (Karte card : hand) {
            if (card.getRang() == rank) {
                return true;
            }
        }
        return false;
    }
    public boolean isEliminated() {
        return !aktiv; // If the player is not active, they are considered eliminated.
    }
    // This method gets the first card in the player's hand, or null if the hand is empty.
    public Karte getHandCard() {
        if (!hand.isEmpty()) {
            return hand.get(0); // Returns the first card
        }
        return null; // No cards in hand
    }

    // This method allows the player to view a specific card's details.
    public void viewCard(Karte card) {
        // Assuming 'card' is not null, print its details.
        if (card != null) {
            System.out.println("Card details: " + card);
        }
    }
    // Methode, um den Spieler als ausgeschieden zu markieren
    public void markiereAlsAusgeschieden() {
        this.istAusgeschieden = true;
    }

    // Methode, um zu prüfen, ob der Spieler ausgeschieden ist
    public boolean istAusgeschieden() {
        return this.istAusgeschieden;
    }


    // Methode, um den Schutzstatus zu setzen
    public void setGeschuetzt(boolean geschuetzt) {
        this.geschuetzt = geschuetzt;
    }

    // Methode, um den Schutzstatus abzurufen
    public boolean isGeschuetzt() {
        return geschuetzt;
    }
    public Karte discardHand() {
        if (!hand.isEmpty()) {
            Karte discardedCard = hand.remove(0); // Assuming one card in hand
            return discardedCard;
        }
        return null; // Return null if hand is empty
    }
    public boolean isInRound() {
        return aktiv && !istAusgeschieden;
    }
    public int getDiscardTotal() {
        int total = 0;
        for (Karte karte : discardPile) {
            total += karte.getRang();
        }
        return total;
    }
    public void discardCard(Karte karte) {
        discardPile.add(karte);
    }
    public void awardToken() {
        this.score += 1;
    }
    public void leereAblagestapel() {
        this.discardPile.clear();
    }

    public void resetZumNeuenRunde(int winningTokensNeeded) {
        hand.clear(); // Clears the hand for the new round.

        // Only set the player to active if they have not won the game.
        if (score < winningTokensNeeded) {
            this.aktiv = true;
        }

        leereAblagestapel(); // Clears the discard pile for the new round.

        this.geschuetzt = false; // Removes protection as the new round starts.
        this.istAusgeschieden = false; // The player is back in the game for the new round.
    }
    public Spieler choosePlayer(List<Spieler> validPlayers) {
        System.out.println("Wähle einen Spieler aus:");
        for (int i = 0; i < validPlayers.size(); i++) {
            System.out.println((i + 1) + ": " + validPlayers.get(i).getName());
        }
        int choice = scanner.nextInt();
        return validPlayers.get(choice - 1); // Adjust for zero-indexed list
    }
    public Karte getCurrentCard() {
        return this.currentCard;
    }
    public void setCurrentCard(Karte currentCard) {
        this.currentCard = currentCard; // Set the new current card
    }



}







